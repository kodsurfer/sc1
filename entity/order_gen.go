package entity


import (
	"fmt"
)


type Order struct{
    BlockchainID string
    State  OrderState `json:"state"`
    ID  string `json:"id"`
    Value  int64 `json:"value"`
    
}

type OrderSearchRequest struct{
    BlockchainID string
    State  *OrderState `json:"state"`
    ID  *string `json:"id"`
    Value  *int64 `json:"value"`
    
}


type OrderState string

const(
    OrderStateConfirmed = "Confirmed"
    OrderStateRejected = "Rejected"
    OrderStateCreated = "Created"
    OrderStateSent = "Sent"
    OrderStateReturned = "Returned"
    
)


// SetStateConfirmed .
func (e *Order) SetStateConfirmed() error {if(e.State !=  OrderStateSent){
            return fmt.Errorf(" Order in state %s can not be set into 'Confirmed' (correct states: [Sent] )", e.State)
    }
    
    e.State = OrderStateConfirmed
    return nil
}

// SetStateRejected .
func (e *Order) SetStateRejected() error {if(e.State !=  OrderStateSent){
            return fmt.Errorf(" Order in state %s can not be set into 'Rejected' (correct states: [Sent] )", e.State)
    }
    
    e.State = OrderStateRejected
    return nil
}

// SetStateCreated .
func (e *Order) SetStateCreated() error {
    e.State = OrderStateCreated
    return nil
}

// SetStateSent .
func (e *Order) SetStateSent() error {if(e.State !=  OrderStateCreated)||(e.State !=  OrderStateReturned){
            return fmt.Errorf(" Order in state %s can not be set into 'Sent' (correct states: [Created Returned] )", e.State)
    }
    
    e.State = OrderStateSent
    return nil
}

// SetStateReturned .
func (e *Order) SetStateReturned() error {if(e.State !=  OrderStateSent){
            return fmt.Errorf(" Order in state %s can not be set into 'Returned' (correct states: [Sent] )", e.State)
    }
    
    e.State = OrderStateReturned
    return nil
}



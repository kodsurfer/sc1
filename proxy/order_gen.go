package proxy

import(
	"sc1/dto"
	"sc1/entity"
    "sc1/api"
	"github.com/hyperledger/fabric-sdk-go/pkg/common/providers/context"
	"github.com/hyperledger/fabric-sdk-go/pkg/client/channel"
	"github.com/hyperledger/fabric-sdk-go/pkg/common/errors/retry"
	"github.com/hyperledger/fabric-sdk-go/pkg/common/providers/fab"
	"encoding/json"
	"fmt"
	"errors"
)

type OrderService struct {
	channelClient   *channel.Client
}


func (svc *OrderService) Order_GetValue(ID string) (int64, error){
	ccRequest,err := MakeChaincodeTransMapRequest("Смартконтракт suffer", []*fab.ChaincodeCall{
			{ID: "Смартконтракт suffer"},
		}, api.Order_GetValue, ID)
	if err != nil{
		return 0,  fmt.Errorf("error creating ccRequest: %s", err)
	}
	var ccResponse channel.Response
	
		ccResponse, err = svc.channelClient.Query(ccRequest, channel.WithRetry(retry.DefaultChannelOpts))
		if err != nil {
			return 0,  fmt.Errorf("failed to query: %s", err)
		}
	

	if ccResponse.ChaincodeStatus != 200 {
		return 0,  errors.New(string(ccResponse.Payload))
	}

	var response dto.GetValueResponse
	err = json.Unmarshal(ccResponse.Payload, &response)
	if err != nil {
		return 0,  fmt.Errorf("failed to parse response payload: %s", err)
	}

	if len(response.Error) != 0{
		return 0,  errors.New(response.Error)
	}

	
    	return response.Result, nil
	}


func NewOrderService(
	chanProv context.ChannelProvider,
) (*OrderService, error) {
	channelClient, err := channel.New(chanProv)
	if err != nil {
		return nil, fmt.Errorf("failed to create channel client: %s", err)
	}
	return &OrderService{
		channelClient: channelClient,
	}, nil
}

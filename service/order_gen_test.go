package service

import (
	"testing"
    "sc1/dto"
	"sc1/repository"
	"github.com/kbkontrakt/hlfabric-ccdevkit/logs"
	. "github.com/smartystreets/goconvey/convey"

	gomock "github.com/golang/mock/gomock"
)

func TestOrderServiceGetValue(t *testing.T) {
	Convey("Order GetValue", t, func(c C) {
		// prepare dummy service .

		ctrl := gomock.NewController(t)
		defer ctrl.Finish()

		svc := NewOrderServiceImpl(
			logs.DummyLogger(),
			repository.NewMockRepository(ctrl),
		)

		c.Convey("Given OrderService", func(c C) {
			c.Convey("When invoking method GetValue", func(c C) {
				// test logic needs to be implemented .
				
				var (
					request    = &dto.GetValueRequest{}
				)
    			c.Convey("It should return error", func(c C) {
					_, err := svc.GetValue(request.ID)
					So(err, ShouldNotBeNil)
				})
			})
		})
	})
}


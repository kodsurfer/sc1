package service

import (
	"errors"
	
	"sc1/repository"
	"sc1/utils/logs"
)

func NewOrderServiceImpl(
	log logs.Logger,
	rep repository.Repository,
) OrderService {
	return &OrderServiceImpl{
		log,
		rep,
	}
}

type OrderServiceImpl struct {
	log logs.Logger
	rep repository.Repository
}

// GetValue .
func (svc *OrderServiceImpl) GetValue(ID string) (int64, error) {
    // implement method logic .
	return 0, errors.New("not implemented")
}


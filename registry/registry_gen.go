package registry

import (
	"github.com/hyperledger/fabric/core/chaincode/shim"
	"sc1/service"
	"sc1/repository"
	"sc1/utils/logs"
)

type (
	// ServiceLocator .
	ServiceLocator interface {
			OrderService() service.OrderService

		Logger() logs.Logger
		Repository() repository.Repository
	}

	serviceLocatorImpl struct {
		stub shim.ChaincodeStubInterface
	}
)

var (
	OrderServiceLog   = shim.NewLogger("OrderService")
)
func (sl *serviceLocatorImpl) OrderService() service.OrderService {
	return service.NewOrderServiceImpl(
		OrderServiceLog,
		sl.Repository(),
		)
}

func (sl *serviceLocatorImpl) Logger() logs.Logger {
	return shim.NewLogger("Смартконтракт suffer")
}

func (sl *serviceLocatorImpl) Repository() repository.Repository {
	return repository.NewRepositoryImpl(shim.NewLogger("Repository"), sl.stub)
}

func NewServiceLocatorImpl(stub shim.ChaincodeStubInterface) ServiceLocator {
	return &serviceLocatorImpl{stub}
}

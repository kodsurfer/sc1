package main

import (
	"encoding/json"
	"errors"
	"fmt"

	"sc1/dto"
	"sc1/registry"
)
// GetValue .
func (chaincode *Смартконтракт sufferChaincode) GetValue(svcFactory registry.ServiceLocator, args []string) ([]byte, error) {
	payload := args[0]
	if len(payload) == 0 {
		return nil, errors.New("empty request payload")
	}

	data := []byte(payload)
	var request dto.GetValueRequest

	err := json.Unmarshal(data, &request)
	if err != nil {
		return nil, fmt.Errorf("failed to parse request payload: %s", err)
	}
	
	result, err := svcFactory.OrderService().GetValue(request.ID)
	if err != nil{
		chaincode.logger.Infof("error invoking method GetValue: %s", err)
	}
	response := dto.GetValueResponse{
	
    	Result: result,
    }
	if err != nil{
		response.Error = err.Error()
	}
	resultData, err := json.Marshal(response)
	if err != nil {
		return nil, err
	}

	return resultData, nil
}




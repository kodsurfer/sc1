package main

import (
	"fmt"

	"github.com/hyperledger/fabric/core/chaincode/shim"
	"github.com/hyperledger/fabric/protos/peer"
	"github.com/kbkontrakt/hlfabric-ccdevkit/debug"
	"github.com/kbkontrakt/hlfabric-ccdevkit/logs"
	"github.com/kbkontrakt/hlfabric-ccdevkit/utils"
	"sc1/api"
	"sc1/registry"
)

const (
	chaincodeVersion      = "0.1.0"
	Смартконтракт sufferCollectionName = "Смартконтракт suffers"
)

// Config .
type Config struct {
	Version     string `json:"version"`
	ChaincodeID string `json:"chaincode_id"`
}

type Смартконтракт sufferChaincode struct {
	logger    logs.Logger
}

func NewСмартконтракт sufferChaincode() *Смартконтракт sufferChaincode {
	chaincode := Смартконтракт sufferChaincode{
		logger:    logs.WithTags(shim.NewLogger("main"), "module", "Смартконтракт suffercc"),
	}

	return &chaincode
}

// Init .
func (chaincode *Смартконтракт sufferChaincode) Init(stub shim.ChaincodeStubInterface) peer.Response {
	logger := chaincode.logger

	fn, args := stub.GetFunctionAndParameters()
	logger = logs.WithTags(logger, "method", "Init", "fn", fn)
	if logger.IsEnabledFor(shim.LogDebug) {
		logger.Debugf("Call with args [%v]", args)
	} else {
		logger.Info("Call")
	}

	return shim.Success(nil)
}

// Invoke .
func (chaincode *Смартконтракт sufferChaincode) Invoke(stub shim.ChaincodeStubInterface) (response peer.Response) {
	logger := chaincode.logger
	fn, args, err := utils.GetFnArgsOrFromTransientMap(stub)
	if err != nil {
		return shim.Error(err.Error())
	}

	logger = logs.WithTags(logger, "method", "Invoke", "fn", fn)

	defer func() {
		if err := recover(); err != nil {
			response = shim.Error("Internal error was occurred, please see log for more details")
			logger.Criticalf("Panic was occurred with [%+v] and stacktrace=[%s]", err, debug.GetStacktrace(false))
		}

		if logger.IsEnabledFor(shim.LogDebug) {
			logger.Debugf("Call with args [%v] and response is status=[%d] msg=[%s] payload=[%s]", args,
				response.GetStatus(),
				response.GetMessage(),
				response.GetPayload())
		} else {
			logger.Infof("Call %d", response.GetStatus())
		}
	}()

	return chaincode.handleByRoute(stub, fn, args)
}

func (chaincode *Смартконтракт sufferChaincode) handleByRoute(stub shim.ChaincodeStubInterface, fn string, args []string) peer.Response {
	svcFactory := registry.NewServiceLocatorImpl(stub)

	var err error
	var payload []byte

	switch fn {
		case api.Order_GetValue:
        payload, err = chaincode.Order_GetValue(svcFactory, args)
    

	case "_debug":
		payload, err = debug.Invoke(stub, args)
	default:
		return shim.Error("unsupported function")
	}

	if err != nil {
		return shim.Error(err.Error())
	}

	return shim.Success(payload)
}

func main() {
	chaincode := NewСмартконтракт sufferChaincode()

	shim.SetupChaincodeLogging()

	chaincode.logger.Info("Start")

	if err := shim.Start(chaincode); err != nil {
		fmt.Printf("Error starting Смартконтракт sufferChaincode: %s", err)
	}
}

package dto

import (
	"sc1/entity"
)


type GetValueRequest struct{
    
    ID string `json:"id"`
    }


type GetValueResponse struct{
    
    Result int64 `json:"result"`
    Error string `json:"error"`
}


type OrderSearchRequest struct{
    State  *entity.OrderState `json:"state"`
    ID  *string `json:"id"`
    Value  *int64 `json:"value"`
    
}
package repository

import (
	"sc1/entity"
	"time"
	"fmt"
	"encoding/json"
	"crypto/sha256"
	"strings"
)


type (
	DocumentType string
)


const (OrderDocumentType = "Order"
	)


// Document .
type Document struct {
	Type DocumentType `json:"type"`
}

type OrderDocument struct{
		Document
		entity.Order
	}
	

func NewOrderDocument(e *entity.Order) OrderDocument{
		timestamp := time.Now().Unix()
		eData, _ := json.Marshal(e)
		h := sha256.Sum256(eData)
		e.BlockchainID = "Order" + fmt.Sprintf("%d", timestamp) + strings.ToUpper(fmt.Sprintf("%x", h[0:4]))
		return OrderDocument{
			Document{
				Type: OrderDocumentType,
			},
			*e,
		}
	}
	
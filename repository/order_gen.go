package repository

import (
	"fmt"
	"errors"
	"encoding/json"
	"sc1/entity"
	"sc1/utils/logs"
	"github.com/hyperledger/fabric/core/chaincode/shim"
)

var (
	ErrOrderNotFound = errors.New("Order not found")
)

type (
	OrderRepository interface {
        New(*entity.Order) (string, error)
        GetByBlockchainID(string) (*entity.Order, error)
		Update(*entity.Order) error
        DeleteByBlockchainID(string) error
        HistoryByBlockchainID(string) ([]entity.Order, error)
        FindItem(string) (*entity.Order, error)
        Find(*entity.OrderSearchRequest) ([]entity.Order, error)
        List() ([]entity.Order, error)
	}

	OrderRepositoryImpl struct {
		log logs.Logger
		stub shim.ChaincodeStubInterface
	}
)

func (rep *OrderRepositoryImpl) New(e *entity.Order) (string, error) {
	log := logs.WithTags(rep.log, "method", "New")

	document := NewOrderDocument(e)

	log.Infof("created entity Order with id %s", document.BlockchainID)

	data, err := json.Marshal(document)
	if err != nil {
		return "", err
	}

	err = rep.stub.PutState(document.BlockchainID, data)
	if err != nil {
		return "", err
	}

	return document.BlockchainID, nil
}

func (rep *OrderRepositoryImpl) GetByBlockchainID(blockchainID string) (*entity.Order, error) {
	log := logs.WithTags(rep.log, "method", "GetByBlockchainID")
	
	log.Infof("searching entity by id %s", blockchainID)

	data, err := rep.stub.GetState(blockchainID)
	if err != nil {
		return nil, err
	}

	if data == nil {
		return nil, ErrOrderNotFound
	}

	document := new(OrderDocument)

	err = json.Unmarshal(data, document)
	if err != nil {
		return nil, err
	}
	
	if document.Type != OrderDocumentType {
		return nil, fmt.Errorf("wrong document type: %s", document.Type)
	}

	return &document.Order, nil
}

func (rep *OrderRepositoryImpl) Update(e *entity.Order) error {
	log := logs.WithTags(rep.log, "method", "Update")
	
	log.Infof("updating entity with id %s", e.BlockchainID)

	document := OrderDocument{
			Document{
				Type: OrderDocumentType,
			},
			*e,
		}

	data, err := json.Marshal(document)
	if err != nil {
		return err
	}

	err = rep.stub.PutState(document.BlockchainID, data)
	if err != nil {
		return err
	}

	return nil
}

func (rep *OrderRepositoryImpl) DeleteByBlockchainID(blockchainID string) error {
	log := logs.WithTags(rep.log, "method", "DeleteByBlockchainID")
	
	log.Infof("deleting entity with id %s", blockchainID)
	
	err := rep.stub.DelState(blockchainID)
	if err != nil {
		return err
	}

	return nil
}

func (rep *OrderRepositoryImpl) HistoryByBlockchainID(blockchainID string) ([]entity.Order, error) {
	log := logs.WithTags(rep.log, "method", "HistoryByBlockchainID")
	
	log.Infof("searching entity history by id %s", blockchainID)
	
	iterator, err := rep.stub.GetHistoryForKey(blockchainID)
	if err != nil {
		return nil, errors.New("failed to excute query: " + err.Error())
	}

	defer iterator.Close()

	var entities []entity.Order

	for iterator.HasNext() {
		entry, err := iterator.Next()
		if err != nil {
			return nil, errors.New("failed to get next entry: " + err.Error())
		}

		var document OrderDocument
		err = json.Unmarshal(entry.Value, &document)
		if err != nil {
			return nil, err
		}
		if document.Type != OrderDocumentType {
			return nil, fmt.Errorf("wrong document type: %s", document.Type)
		}

		entities = append(entities, document.Order)
	}

	return entities, nil

}

func (rep *OrderRepositoryImpl) List() ([]entity.Order, error) {
	log := logs.WithTags(rep.log, "method", "List")
	
	log.Infof("getting all Order entities")

	query := fmt.Sprintf(`{"selector":{"type":"%s"}}`, OrderDocumentType)

	iterator, err := rep.stub.GetQueryResult(query)
	if err != nil {
		return nil, errors.New("failed to excute query: " + err.Error())
	}

	defer iterator.Close()

	var entities []entity.Order

	for iterator.HasNext() {
		entry, err := iterator.Next()
		if err != nil {
			return nil, errors.New("failed to get next entry: " + err.Error())
		}

		var document OrderDocument
		err = json.Unmarshal(entry.Value, &document)
		if err != nil {
			return nil, err
		}
		if document.Type != OrderDocumentType {
			return nil, fmt.Errorf("wrong document type: %s", document.Type)
		}

		entities = append(entities, document.Order)
	}

	return entities, nil
}

func (rep *OrderRepositoryImpl) FindItem(query string) (*entity.Order, error) {
	log := logs.WithTags(rep.log, "method", "FindItem")
	
	log.Infof("finding entity item by query")
	
	iterator, err := rep.stub.GetQueryResult(query)
	if err != nil {
		return nil, errors.New("failed to excute query: " + err.Error())
	}

	defer iterator.Close()

	var entity *entity.Order

	for iterator.HasNext() {
		entry, err := iterator.Next()
		if err != nil {
			return nil, errors.New("failed to get next entry: " + err.Error())
		}

		var document OrderDocument
		err = json.Unmarshal(entry.Value, &document)
		if err != nil {
			return nil, err
		}
		if document.Type != OrderDocumentType {
			return nil, fmt.Errorf("wrong document type: %s", document.Type)
		}

		entity = &document.Order
	}

	if entity == nil {
		return nil, ErrOrderNotFound
	}

	return entity, nil
}

func (rep *OrderRepositoryImpl) Find(req *entity.OrderSearchRequest) ([]entity.Order, error) {
	log := logs.WithTags(rep.log, "method", "FindItem")
	
	log.Infof("finding entity item by search request %+v", req)
	
	querySelector := map[string]interface{}{"type": OrderDocumentType}
	
	if req.State != nil{
		querySelector["State"] = *req.State
	}
    if req.ID != nil{
		querySelector["ID"] = *req.ID
	}
    if req.Value != nil{
		querySelector["Value"] = *req.Value
	}
    

	query, err := json.Marshal(querySelector)
	if err != nil {
		return nil, fmt.Errorf("failed to format query: %s", err)
	}

	iterator, err := rep.stub.GetQueryResult(string(query))
	if err != nil {
		return nil, errors.New("failed to excute query: " + err.Error())
	}

	defer iterator.Close()

	var entities []entity.Order

	for iterator.HasNext() {
		entry, err := iterator.Next()
		if err != nil {
			return nil, errors.New("failed to get next entry: " + err.Error())
		}

		var document OrderDocument
		err = json.Unmarshal(entry.Value, &document)
		if err != nil {
			return nil, err
		}
		if document.Type != OrderDocumentType {
			return nil, fmt.Errorf("wrong document type: %s", document.Type)
		}

		entities = append(entities, document.Order)
	}

	return entities, nil
}

func NewOrderRepositoryImpl(
	log logs.Logger,
	stub shim.ChaincodeStubInterface,
) OrderRepository {
	return &OrderRepositoryImpl{
		log: log,
		
		stub: stub,
    	}
}

package repository

import(
	"sc1/utils/logs"
	"github.com/hyperledger/fabric/core/chaincode/shim"
)

type (
	Repository interface{
		OrderRepository() OrderRepository
		}

	repositoryImpl struct {
		log logs.Logger
		stub shim.ChaincodeStubInterface
	}
)

func (rep *repositoryImpl)OrderRepository() OrderRepository{
	return NewOrderRepositoryImpl(logs.WithTags(rep.log, "entity", "Order"), rep.stub)
}
func NewRepositoryImpl(
	log logs.Logger,
	stub shim.ChaincodeStubInterface,
) Repository {
	return &repositoryImpl{
		log: log,
		stub: stub,
	}
}
